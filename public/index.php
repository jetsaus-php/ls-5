<?php

require_once __DIR__ . '/../config/config.php';

/*
// создание подключения к БД
$link = mysqli_connect('ls', 'root', 'opdf115', 'gb');
if (!$link) {
    echo 'Подключение не произошло';
    exit();
}

//var_dump($link);
echo 'Подключение успешно';

// лечение кодировки
mysqli_set_charset($link, 'utf8');

// объект для выборки
$result = mysqli_query($link, 'SELECT * FROM `users`');

// выборка
$rows = [];
while ($row = mysqli_fetch_assoc($result)) {
    $rows[] = $row;
}

// закрытие
mysqli_close($link);


// вывод данных
var_dump($rows);
*/

$news = getNews();
$content = renderNews($news);
echo render(TEMPLATES_DIR . 'index.tpl', [
    'title' => 'Новости',
    'h1' => 'Горячие новости',
    'content' => $content
]);