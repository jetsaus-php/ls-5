<?php
/*
 * Функции для работы с БД
 */

    /*
     * Создание соединения с БД
     * Вход:    параметры подключения из config.php
     * Возврат: $db - ссылка на соединение
     */
    function createConnection()
    {
        $db = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        mysqli_query($db, "SET CHARACTER SET 'utf8'");              // Установка кодировки UTF-8
        return $db;
    }

    /*
     * Получение объекта для выполнения SQL-команды
     * Вход:    $sql - SQL-команда
     * Возврат: $result - объект для SQL-команды
     */
    function execQuery($sql)
    {
        $db = createConnection();
        $result = mysqli_query($db, $sql);
        mysqli_close($db);
        return $result;
    }

    /*
     * Получение результата SQL-команды:
     * 1.Создает соединение
     * 2.Выполняет SQL-команду
     * 3.Создает буфер
     * 4.Записывает данные, возвращаемые SQL-командой в буфер
     * 5.Возвращает буфер с данными как результат выполнения функции
     * 6.Закрывает соединение с БД
     *
     * Вход:    $sql - SQL-команда
     * Возврат: $arrayResult - массив с результатом SQL-команды
     */
    function getAssocResult($sql)
    {
        $db = createConnection();
        $result = mysqli_query($db, $sql);
        $arrayResult = [];
        while($row = mysqli_fetch_assoc($result)) {
            $arrayResult[] = $row;
        }
        mysqli_close($db);
        return $arrayResult;
    }

    /*
     * Получение первого элемента из выборки
     * Вход:    $sql - SQL-запрос
     * Возврат: $result - массив с результатом SQL-команды
     */
    function getFirst($sql)
    {
        $result = getAssocResult($sql);
        if (empty($result)) {
            return NULL;
        }
        return $result[0];
    }